angularApp = angular.module('mainApp', []);

angularApp.controller('mainController', function() {
    var mainCtrl = this;
    mainCtrl.lists = [
      {"text": ["Área de construção diferente do tamanho do lote, rever escala.", "Resposta aqui."], "done": false, "check":false}
    ];
    mainCtrl.add = function() {
      if (!mainCtrl.new) return;
      mainCtrl.lists.push({"text": [mainCtrl.new], "done": false, "check":false});
      mainCtrl.new = "";
    };
    mainCtrl.doneCount = function() {
        var count = 0; mainCtrl.lists.forEach(function(list) {
        if (list.done) count++;
      });
      return count;
    };
    mainCtrl.deleteDone = function() {
      var oldLists = mainCtrl.lists;
      mainCtrl.lists = []; angular.forEach(oldLists, function(list) {
      if (!list.done) {
        mainCtrl.lists.push(list);
      }
    });
  };
  });